import Vue from 'vue';
import router from './router';
import App from './App.vue'
import filters from './filters/filters';
import store from './store/store';

for(let name in filters) {
    Vue.filter(name, filters[name]);
}

new Vue({
	el: '#app',
	router,
	render: h => h(App),
});

router.beforeResolve(async (to, from, next) => {
	await store.initData()
	next();
});
