export default {
	removeEmailDomain: email => email.split('@')[0],
	pluralize: (singularStr, amount, prefixAmount) => {
		var string = amount === 1 ? singularStr : singularStr + 's';
		return (prefixAmount ? amount + ' ' : '') + string;
	}
}
