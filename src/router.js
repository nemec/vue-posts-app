import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

function loadView(view) {
	return () => import(`./pages/${view}.vue`);
}

const routes = [
	{ path: '*', beforeEnter: (to, from, next) => { next('/404') } },
	{
		path: '/',
		redirect: '/posts'
	},
	{
		path: '/404',
		name: '404',
		component: loadView('404')
	},
	{
		path: '/posts',
		name: 'Posts',
		component: loadView('Posts')
	},
	{
		path: '/post/:id',
		name: 'Post',
		props: true,
		component: loadView('Post')
	}
];

const router = new VueRouter({
	mode: 'history',
	routes
})

Vue.router = router;

export default router;
