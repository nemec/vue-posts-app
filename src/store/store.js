var api = {
	url : 'https://jsonplaceholder.typicode.com/',
	endpoints : ['posts', 'comments', 'users']
};

var store = {
	state: {
		posts: null
	},
	async initData() {
		if (this.state.posts != null) return;
		var data = {};
		this.state.posts = [];
		let jsonArray = await Promise.all(api.endpoints.map(endpoint => fetch(api.url + endpoint))).then(async res => {
			return Promise.all(res.map(async data => await data.json()));
		});
		for(var [index, endpoint] of api.endpoints.entries()) {
			data[endpoint] = jsonArray[index];
		}
		for(var post of data.posts) {
			post['comments'] = data.comments.filter(u => u.postId == post.id);
			post['user'] = data.users.find(u => u.id == post.userId);
			this.state.posts.push(post)
		}
	}
}

export default store;
